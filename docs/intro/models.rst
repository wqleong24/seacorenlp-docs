#################
Model Performance
#################

This page presents a summary of the performance of our natively trained models
as well as the third-party models that we integrate into our library.

.. note::
   Certain packages (such as Trankit) have been included for completeness of information in comparing
   model performance but are not integrated into our library. These have been marked with an asterisk ``*``.

.. note::
   Models from the Malaya package generally cannot be compared directly to other models as the datasets
   used both for training and testing have been augmented and use variable split ratios.

   Furthermore, Malaya models have the option to be quantized which reduces file size and increases
   loading/inference speed, but causes a dip in performance.

************
Tokenization
************

.. csv-table::
   :file: tables/tokenizers.csv
   :header-rows: 1

*********************
Sentence Segmentation
*********************

.. csv-table::
   :file: tables/sentence-segmenters.csv
   :header-rows: 1

.. [#f1] Refer to original `CRFCut Github <https://github.com/vistec-AI/crfcut>`_
   for more details on performance when trained and tested on different datasets.

**********************
Part-of-speech Tagging
**********************

We only support UPOS tagging at the moment for natively trained models.

.. csv-table::
   :file: tables/pos-taggers.csv
   :header-rows: 1

************************
Named Entity Recognition
************************

.. csv-table::
   :file: tables/ner-taggers.csv
   :header-rows: 1

********************
Constituency Parsing
********************

.. csv-table::
   :file: tables/constituency-parsers.csv
   :header-rows: 1

.. [#f2] This architecture comprises embeddings (XLM-R Base in our case)
   followed by a variable number of self-attention layers. Refer to the original paper
   `"Multilingual Constituency Parsing with Self-Attention and Pre-Training (2018)"
   <https://arxiv.org/abs/1812.11760>`_ or this
   `Github <https://github.com/nikitakit/self-attentive-parser>`_ for more details.

.. [#f3] Credits to Jessica Naraiswari Arwidarasti, Ika Alfina and Dr Adila Alfa Krisnadhi at
   Universitas Indonesia for the great work in producing this open-source constituency treebank
   for Indonesian. Please refer to their paper `"Converting an Indonesian Constituency Treebank
   to the Penn Treebank Format (2019)" <https://ieeexplore.ieee.org/abstract/document/9037723>`_
   for more details.

.. [#f4] The previous state-of-the-art benchmark for constituency parsing in Indonesian was
   an F1 score of 70.90% using StanfordCoreNLP's Shift-Reduce Parser. Please refer to their
   original paper `"Converting an Indonesian Constituency Treebank
   to the Penn Treebank Format (2019)" <https://ieeexplore.ieee.org/abstract/document/9037723>`_
   for more details.

.. [#f5] This architecture comprises embeddings (XLM-R Base in our case) followed by a
   bi-directional LSTM, followed by a bi-directional span extraction layer and ending with a
   feedforward neural network. Please refer to AllenNLP's original paper `"Extending a Parser
   to Distant Domains Using a Few Dozen Partially Annotated Examples (2018)"
   <https://arxiv.org/abs/1805.06556>`_ for more details.

******************
Dependency Parsing
******************

.. csv-table::
   :file: tables/dependency-parsers.csv
   :header-rows: 1

.. [#f6] Please refer to Timothy Dozat and Christopher Manning's original paper
   `"Deep Biaffine Attention for Neural Dependency Parsing (2017)"
   <https://arxiv.org/pdf/1611.01734.pdf>`_ for more details on this architecture.

.. [#f7] The scores displayed here under `UAS` and `LAS` for the Malaya models
   are reported as `Arc Accuracy` and `Types Accuracy` in the official `Malaya documentation
   <https://malaya.readthedocs.io/en/latest/load-dependency.html>`_. We believe that they
   correspond and have therefore reported them as `UAS` and `LAS` in order to standardize
   the way we report metrics, but it is unclear what the author of the documentation
   meant exactly by these terms.
