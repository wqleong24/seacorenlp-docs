############
Installation
############

.. code-block:: shell

   pip install seacorenlp

If you wish to make use of Stanza models, ensure that you also install the relevant models
after installation of our ``seacorenlp`` package.

.. code-block:: python

   import stanza

   stanza.download('id') # Download Indonesian models
   stanza.download('vi') # Download Vietnamese models
