********************
Reference Literature
********************

This section lists out some of the literature that we consulted in our work in one way or another.

1. `Jwalapuram, P., Joty, S., and Shen, S. (2020). Pronoun-Targeted Fine-tuning for NMT with Hybrid Losses. Proceedings of the 2020 Conference on Empirical Methods in Natural Language Processing, November 2020 (pp. 2267-2279). <https://aclanthology.org/2020.emnlp-main.177.pdf>`_
2. `Mohiuddin, T., Bari, M. S., and Joty, S (2020). LNMap: Departures from Isomorphic Assumption in Bilingual Lexicon Induction Through Non-Linear Mapping in Latent Space. Proceedings of the 2020 Conference on Empirical Methods in Natural Language Processing, November 2020 (pp. 2712-2723). <https://aclanthology.org/2020.emnlp-main.215.pdf>`_
