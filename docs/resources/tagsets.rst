##############
Corpus Tagsets
##############

This section explains the tagsets used in various corpora.
We have grouped them by task and we also provide links to relevant sources where applicable.

**********************
Part-of-speech Tagging
**********************

Universal Dependencies UPOS
===========================

Further details on the definition of these POS tags can be found on the
`Universal Dependencies website <https://universaldependencies.org/u/pos/index.html>`_.

.. csv-table::
   :file: tables/ud-pos.csv
   :header-rows: 1


Thai - ORCHID Corpus XPOS
=========================

The following definitions were extracted from the original paper
`ORCHID: Thai Part-Of-Speech Tagged Corpus <https://docs.wixstatic.com/ugd/cdb1d4_c0ff5a3e6f824630a1f5cbef12593292.pdf>`_
published in 2009.

.. csv-table::
   :file: tables/orchid-xpos.csv
   :header-rows: 1

Vietnamese XPOS (Underthesea)
=============================

The following is the XPOS tagset used by the ``underthesea`` package for their POS Tagger.
While it is not stated explicitly what corpus their model was trained on, we managed to extract the labels
from their model.

XPOS tagsets seem to vary from paper to paper although there are many similarities. None
of the papers had the exact tagset used by ``underthesea`` model. Therefore, we decided to
synthesize the tagset information ourselves by combing a selection of such papers
relating to Vietnamese treebanks.

Some of the papers included are:

1. `Utilizing State-of-the-art Parsers to Diagnose Problems in Treebank Annotation for a Less Resourced Language <https://aclanthology.org/W13-2303.pdf>`_
2. `From Treebank Conversion to Automatic Dependency Parsing for Vietnamese <https://www.researchgate.net/publication/279916415_From_Treebank_Conversion_to_Automatic_Dependency_Parsing_for_Vietnamese>`_

.. csv-table::
   :file: tables/vi-xpos.csv
   :header-rows: 1


********************
Constituency Parsing
********************

Please refer to the `Penn Treebank Bracketing Guidelines <http://languagelog.ldc.upenn.edu/myl/PennTreebank1995.pdf>`_
for more information on the constituent tagsets.

******************
Dependency Parsing
******************

Please refer to the `Universal Dependencies website <https://universaldependencies.org/u/dep/index.html>`_
for more details on dependency relation tags.
