SEACoreNLP Documentation
========================

Welcome to SEACoreNLP's documentation page!

.. toctree::
   :maxdepth: 1
   :caption: Introduction

   docs/intro/introduction.rst
   docs/intro/installation.rst
   docs/intro/quickstart.rst
   docs/intro/cli.rst
   docs/intro/models.rst

.. toctree::
   :maxdepth: 1
   :caption: Usage

   docs/usage/data.rst
   docs/usage/pos.rst
   docs/usage/ner.rst
   docs/usage/cp.rst
   docs/usage/dp.rst

.. toctree::
   :maxdepth: 1
   :caption: Resources

   docs/resources/datasets.rst
   docs/resources/packages.rst
   docs/resources/tagsets.rst
   docs/resources/papers.rst
