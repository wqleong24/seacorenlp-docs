from typing import List

from allennlp.common.util import JsonDict
from allennlp.data import Instance
from allennlp.predictors import Predictor

from seacorenlp.data.tokenizers import Tokenizer
from seacorenlp.tagging.pos.training.dataset_reader import UDPOSReader
from seacorenlp.tagging.pos.training.model import POSLSTMTagger, POSTransformerTagger

class POSPredictor(Predictor):
    def __init__(self, model_path: str, lang: str) -> None:
        pass

    def predict(self, sentence: str) -> List[str]:
        pass

    def _json_to_instance(self, json_dict: JsonDict) -> Instance:
        pass
