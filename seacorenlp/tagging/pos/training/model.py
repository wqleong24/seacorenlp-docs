import torch

from typing import Dict

from allennlp.models import Model
from allennlp.data import Vocabulary
from allennlp.modules import TextFieldEmbedder, Seq2SeqEncoder

class POSLSTMTagger(Model):
    def __init__(self,
                 vocab: Vocabulary,
                 embeddings: TextFieldEmbedder,
                 encoder: Seq2SeqEncoder):
        super().__init__(vocab)
        self.embeddings, self.encoder = embeddings, encoder
        num_labels = vocab.get_vocab_size("labels")
        self.tagger = torch.nn.Linear(encoder.get_output_dim(), num_labels)
        self.accuracy = CategoricalAccuracy()

    def forward(self,
                tokens: Dict[str, torch.Tensor],
                tags: torch.Tensor = None) -> torch.Tensor:

        embedded_sentence = self.embeddings(tokens)
        mask = get_text_field_mask(tokens)
        encoded_sentence = self.encoder(embedded_sentence, mask)
        logits = self.tagger(encoded_sentence)

        output = {'logits': logits}
        if tags is not None:
            self.accuracy(logits, tags, mask)
            output['loss'] = sequence_cross_entropy_with_logits(
                logits, tags, mask)
        return output

    def get_metrics(self, reset: bool = False) -> Dict[str, float]:
        return {"accuracy": self.accuracy.get_metric(reset)}


class POSTransformerTagger(Model):
    def __init__(self,
                 vocab: Vocabulary,
                 embeddings: TextFieldEmbedder):
        super().__init__(vocab)
        self.embeddings = embeddings
        num_labels = vocab.get_vocab_size('labels')
        self.tagger = torch.nn.Linear(embeddings.get_output_dim(), num_labels)
        self.accuracy = CategoricalAccuracy()

    def forward(self,
                tokens: Dict[str, torch.Tensor],
                tags: torch.Tensor = None,
                **args) -> Dict[str, torch.Tensor]:

        embedded_sentence = self.embeddings(tokens)
        mask = get_text_field_mask(tokens)
        logits = self.tagger(embedded_sentence)

        output = {"logits": logits}

        if tags is not None:
            self.accuracy(logits, tags, mask)
            output["loss"] = sequence_cross_entropy_with_logits(logits, tags, mask)

        return output

    def get_metrics(self, reset: bool = False) -> Dict[str, float]:
        return {"accuracy": self.accuracy.get_metric(reset)}
