from typing import List, Tuple

import stanza
from stanza.models.common.doc import Document
from allennlp.predictors import Predictor


class StanzaPOSTagger(Predictor):
    """An AllenNLP Predictor that makes use of Stanza POS taggers."""
    VALID_LANGUAGES = {'id', 'ta', 'vi'}

    def __init__(self, lang: str, **kwargs):
        assert lang in StanzaPOSTagger.VALID_LANGUAGES, \
            f'Language {lang} not supported for Stanza POS Tagging!'

        if lang == 'vi':
            # Replace default Stanza Vietnamese tokenizer with UnderTheSea's
            # as it does not work well especially with longer sentences.
            processors = {'tokenize': 'underthesea-tokenize'}
        else:
            processors = 'tokenize, pos'

        self.nlp = stanza.Pipeline(lang=lang,
                                   processors=processors,
                                   tokenize_no_ssplit=True,
                                   **kwargs)

    def _extract_pos_from_stanza_doc(self, doc: Document) -> List[Tuple[str, str]]:
        pos_tags = [(word.text, word.upos)
                    for sent in doc.sentences for word in sent.words]
        return pos_tags

    def predict(self, text: str) -> List[Tuple[str, str]]:
        doc = self.nlp(text)
        pos_tags = self._extract_pos_from_stanza_doc(doc)
        return pos_tags
