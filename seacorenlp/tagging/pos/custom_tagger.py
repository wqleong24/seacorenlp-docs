from typing import List, Tuple
from allennlp.predictors import Predictor

# This file contains a limited set of POS Taggers imported from external libraries
# We support only Thai (PyThaiNLP), Malay (Malaya) and Vietnamese (UnderTheSea) at the moment.


class CustomPOSTagger(Predictor):
    def __init__(self, lang: str, engine: str = None, corpus: str = None, tokenizer: str = None):
        self.lang = lang
        self.engine = engine
        self.corpus = corpus
        self.tokenizer = tokenizer

    def predict(self, text: str) -> List[Tuple[str, str]]:
        pass


class ThaiPOSTagger(CustomPOSTagger):
    """
    AllenNLP Predictor wrapped around PyThaiNLP's POS Tagger.
    Two models are available for use: Averaged Perceptron & Unigram Tagger
    """

    def __init__(self, engine: str = 'perceptron', corpus: str = 'orchid_ud', tokenizer: str = 'attacut'):
        """
        Initialize a PyThaiNLP POS Tagger, specifying the tokenizer, engine and training corpus.

        Tokenizer: Maximum Matching ('newmm') or Attacut ('attacut')
        Engine: Averaged Perceptron ('perceptron') or Unigram Tagger ('unigram') or RDRPOSTagger('artagger')
        Corpus: Orchid ('orchid'), Orchid UD ('orchid_ud'), Parallel UD ('pud') or LST20 ('lst20')
        """
        VALID_TOKENIZERS = {'newmm', 'attacut'}
        VALID_ENGINES = {'perceptron', 'unigram', 'artagger'}
        VALID_CORPORA = {'orchid', 'orchid_ud', 'pud', 'lst20'}
        assert tokenizer in VALID_TOKENIZERS, f'{tokenizer} is not a valid tokenizer.'
        assert engine in VALID_ENGINES, f'{engine} is not a valid engine.'
        assert corpus in VALID_CORPORA, f'{corpus} is not a valid corpus.'

        pass

    def predict(self, text: str) -> List[Tuple[str, str]]:
        pass


class MalayPOSTagger(CustomPOSTagger):
    """
    AllenNLP Predictor wrapped around Malaya's POS Tagger (BERT-based).
    """

    def __init__(self, engine: str = 'alxlnet', quantized: bool = False):
        """
        Initialize a Malaya POS Tagger, specifying the BERT model to be used.

        Models: 'bert', 'tiny-bert', 'albert', 'tiny-albert', 'xlnet', 'alxlnet'
        """
        VALID_ENGINES = {'bert', 'tiny-bert', 'albert',
                         'tiny-albert', 'xlnet', 'alxlnet'}
        assert engine in VALID_ENGINES, f'{engine} is not a valid engine.'

        pass

    def predict(self, text: str) -> List[Tuple[str, str]]:
        pass


class VietnamesePOSTagger(CustomPOSTagger):
    """
    AllenNLP Predictor wrapped around UnderTheSea's POS Tagger. (CRF-based)
    """

    def __init__(self):
        pass

    def predict(self, text: str) -> List[Tuple[str, str]]:
        pass
