"""
Module for Part-of-speech Tagging
"""

from seacorenlp.models import BasePredictor
from seacorenlp.tagging.pos.training import POSPredictor
from seacorenlp.tagging.pos.custom_tagger import (
    MalayPOSTagger, ThaiPOSTagger, VietnamesePOSTagger
)
from seacorenlp.tagging.pos.stanza_tagger import StanzaPOSTagger


class POSTagger(BasePredictor):
    """
    Base class to instantiate specific POSTagger (AllenNLP Predictor)

    **Options for model_name:**
      * E.g. ``pos-th-ud-xlmr``
      * Refer to table containing POS Tagger performance for full list

    **Options for library_name:**
      * ``malaya`` (For Indonesian/Malay)
      * ``stanza`` (For Indonesian and Vietnamese)
      * ``pythainlp`` (For Thai)
      * ``underthesea`` (For Vietnamese)

    **Defaults available for the following languages:**
      * ``id``: Indonesian
      * ``ms``: Malay
      * ``th``: Thai
      * ``vi``: Vietnamese
    """
    TASK = 'pos'
    PREDICTOR_CLASS = POSPredictor
    EXTERNAL_LIBRARIES = {
        'malaya': MalayPOSTagger,
        'pythainlp': ThaiPOSTagger,
        'stanza': StanzaPOSTagger,
        'underthesea': VietnamesePOSTagger
    }
    DEFAULTS = {
        'id': {'class': StanzaPOSTagger, 'kwargs': {'lang': 'id'}},
        'ms': {'class': MalayPOSTagger, 'kwargs': {}},
        'th': {'class': ThaiPOSTagger, 'kwargs': {}},
        'vi': {'class': VietnamesePOSTagger, 'kwargs': {}}
    }
