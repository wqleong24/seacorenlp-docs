
from typing import List

from allennlp.common.util import JsonDict
from allennlp.data import Instance
from allennlp.predictors import Predictor

from seacorenlp.data.tokenizers import Tokenizer
from seacorenlp.tagging.ner.training.dataset_readers import NERGritDatasetReader, ThaiNERDatasetReader


class NERPredictor(Predictor):
    def __init__(self, model_path: str, lang: str) -> None:
        archive = load_archive(model_path)
        super().__init__(archive.model, archive.dataset_reader)
        self._tokenizer = Tokenizer.from_default(lang)

    def predict(self, sentence: str) -> List[str]:
        tokens = [token.text for token in self._tokenizer.tokenize(sentence)]
        logits = self.predict_json({"sentence": sentence})['logits']
        tag_ids = np.argmax(logits, axis=-1)
        ner_tags = [self._model.vocab.get_token_from_index(
            id, 'labels') for id in tag_ids]
        return list(zip(tokens, ner_tags))

    def _json_to_instance(self, json_dict: JsonDict) -> Instance:
        tokens = self._tokenizer.tokenize(json_dict["sentence"])
        return self._dataset_reader.text_to_instance(tokens)
