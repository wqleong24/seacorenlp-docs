"""
Module for Named Entity Recognition
"""

from seacorenlp.models import BasePredictor
from seacorenlp.tagging.ner.training import NERPredictor
from seacorenlp.tagging.ner.custom_tagger import (
    MalayNERTagger, ThaiNERTagger, VietnameseNERTagger
)


class NERTagger(BasePredictor):
    """
    Base class to instantiate specific NERTagger (AllenNLP Predictor)

    **Options for model_name:**
      * E.g. ``ner-th-thainer-xlmr``
      * Refer to table containing NER Tagger performance for full list

    **Options for library_name:**
      * ``malaya`` (For Indonesian/Malay)
      * ``pythainlp`` (For Thai)
      * ``underthesea`` (For Vietnamese)
    """
    TASK = 'ner'
    PREDICTOR_CLASS = NERPredictor
    EXTERNAL_LIBRARIES = {
        'malaya': MalayNERTagger,
        'underthesea': VietnameseNERTagger,
        'pythainlp': ThaiNERTagger
    }
