from typing import List
from nltk.tree import Tree
from allennlp.predictors import Predictor
from seacorenlp.data.tokenizers import SentenceSplitter, Tokenizer

class Parser:
    pass

class BeneparConstituencyParser(Predictor):
    def __init__(self, model_path: str, lang: str) -> None:
        self.parser = Parser(model_path)
        self.lang = lang
        self._tokenizer = Tokenizer.from_default(lang)
        self._sentence_splitter = SentenceSplitter.from_default(lang)

    def predict(self, text: str) -> List[Tree]:
        sentences = self._sentence_splitter.split_sentences(text)
        tokenized_sentences = [
            [token.text for token in self._tokenizer.tokenize(sent)] for sent in sentences]
        return [self.parser.parse(sent) for sent in tokenized_sentences]
