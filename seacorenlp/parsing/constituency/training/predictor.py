from typing import List
from nltk import Tree
from allennlp.common.util import JsonDict
from allennlp.data import Instance
from allennlp.predictors import Predictor
from seacorenlp.data.tokenizers import SentenceSplitter, Tokenizer
from seacorenlp.tagging.pos import POSTagger


class ConstituencyParserPredictor(Predictor):
    VALID_LANGUAGES = {'id', 'ms', 'th', 'vi'}
    VALID_POS_LANGUAGES = {'id', 'ms', 'th', 'vi'}

    def __init__(self, model_path: str, lang: str, predict_pos: bool = False) -> None:
        assert lang in ConstituencyParserPredictor.VALID_LANGUAGES, \
            f'Language {lang} is not supported for tokenization.'
        if predict_pos:
            assert lang in ConstituencyParserPredictor.VALID_POS_LANGUAGES, \
                f'Language {lang} not supported for POS tagging. Set predict_pos to False.'

        archive = load_archive(model_path)
        super().__init__(archive.model, archive.dataset_reader)
        self._dataset_reader._use_pos_tags = predict_pos
        self._tokenizer = Tokenizer.from_default(lang)
        self._sentence_splitter = SentenceSplitter.from_default(lang)
        self._pos_tagger = POSTagger.from_default(lang) if predict_pos else None

    def predict(self, text: str) -> List[Tree]:
        sentences = self._sentence_splitter.split_sentences(text)
        return [self.predict_json({"sentence": sent}) for sent in sentences]

    def predict_instance(self, instance: Instance) -> Tree:
        outputs = self._model.forward_on_instance(instance)
        return outputs['trees']

    def _json_to_instance(self, json_dict: JsonDict) -> Instance:
        tokens = self._tokenizer.tokenize(json_dict["sentence"])
        # Grab POS tag from word-POS tuples
        pos_tags = [tuple[1] for tuple in self._pos_tagger.predict(
            json_dict["sentence"])] if self._pos_tagger else None
        return self._dataset_reader.text_to_instance([token.text for token in tokens], pos_tags)
