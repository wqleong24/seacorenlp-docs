"""
Module for Constituency Parsing
"""

from typing import Union

from seacorenlp.models import BasePredictor
from seacorenlp.utils import (
    _check_if_model_is_valid, _check_if_task_is_valid, download_model_if_absent
)
from seacorenlp.parsing.constituency.custom_parser import IndonesianConstituencyParser
from seacorenlp.parsing.constituency.benepar_parser import BeneparConstituencyParser
from seacorenlp.parsing.constituency.training import ConstituencyParserPredictor

class ConstituencyParser(BasePredictor):
    """
    Base class to instantiate specific ConstituencyParser (AllenNLP Predictor)

    **Options for model_name:**
      * E.g. ``cp-id-kethu-xlmr``
      * Refer to table containing Constituency Parser performance for full list

    **Options for library_name:**
      * ``malaya`` (For Indonesian/Malay)
    """
    TASK = 'cp'
    EXTERNAL_LIBRARIES = {'malaya': IndonesianConstituencyParser}

    @classmethod
    def from_pretrained(cls, model_name: str) -> Union[BeneparConstituencyParser, ConstituencyParserPredictor]:
        """
        Returns a natively trained ConstituencyParser based on the model name provided

        Args:
            model_name (str): Name of the model

        Returns:
            Predictor: An AllenNLP Predictor that performs constituency parsing
        """
        _check_if_model_is_valid(model_name)
        task, lang = model_name.split('-')[:2]
        _check_if_task_is_valid(task, cls.TASK)
        download_model_if_absent(model_name)

        if 'benepar' in model_name:
            return BeneparConstituencyParser(model_name, lang)
        else:
            return ConstituencyParserPredictor(f'{model_name}.tar.gz', lang)
