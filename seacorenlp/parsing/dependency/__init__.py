"""
Module for Dependency Parsing
"""

from seacorenlp.models import BasePredictor
from seacorenlp.parsing.dependency.custom_parser import (
    IndonesianDependencyParser, ThaiDependencyParser, VietnameseDependencyParser
)
from seacorenlp.parsing.dependency.training import DependencyParserPredictor


class DependencyParser(BasePredictor):
    """
    Base class to instantiate specific DependencyParser (AllenNLP Predictor)

    **Options for model_name:**
      * E.g. ``dp-th-ud-xlmr``
      * Refer to table containing Dependency Parser performance for full list

    **Options for library_name:**
      * ``malaya`` (For Indonesian/Malay)
      * ``pythainlp`` (For Thai)
      * ``underthesea`` (For Vietnamese)
    """
    TASK = 'dp'
    PREDICTOR_CLASS = DependencyParserPredictor
    EXTERNAL_LIBRARIES = {
        'malaya': IndonesianDependencyParser,
        'pythainlp': ThaiDependencyParser,
        'underthesea': VietnameseDependencyParser
    }
