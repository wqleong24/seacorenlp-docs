from typing import Union

from allennlp.data.tokenizers.tokenizer import Tokenizer
from allennlp.data.tokenizers.sentence_splitter import SentenceSplitter


class BaseSegmenter:
    EXTERNAL_LIBRARIES: dict = {}
    DEFAULTS: dict = {}

    @classmethod
    def from_library(cls, library_name: str, **kwargs) -> Union[Tokenizer, SentenceSplitter]:
        """
        Returns a third-party segmenter based on the name of the library provided.

        Keyword arguments can be passed as necessary to specify the corpora and
        engines etc. for the third-party segmenter.

        Args:
            library_name: Name of third-party library
            **kwargs: Additional keyword arguments specific to each library
        """
        assert library_name in cls.EXTERNAL_LIBRARIES.keys(), \
            f'{library_name} is not a valid library name.'
        return cls.EXTERNAL_LIBRARIES[library_name](**kwargs)

    @classmethod
    def from_default(cls, lang: str) -> Union[Tokenizer, SentenceSplitter]:
        """
        Returns a default segmenter based on the language specified.

        Args:
            lang: The 2-letter ISO 639-1 code of the desired language
        """
        config = cls.DEFAULTS.get(lang, None)
        if not config:
            raise ValueError(
                f'Language {lang} is not supported for segmentation.')
        return config['class'](**config['kwargs'])
