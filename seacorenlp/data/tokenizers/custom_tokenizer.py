from typing import List

from allennlp.data.tokenizers.token_class import Token
from allennlp.data.tokenizers.tokenizer import Tokenizer


class ThaiTokenizer(Tokenizer):
    VALID_TOKENIZER_ENGINE = {'attacut', 'newmm'}
    """
    An AllenNLP tokenizer that makes use of PyThaiNLP's word tokenizing engine.
    Two options are available for the engine:
    'attacut': Default tokenizer. Good balance of accuracy and speed.
    'newmm': Dictionary-based, uses TCC and maximum matching, may produce longer tokens.
    """

    def __init__(self, engine: str = 'attacut') -> None:
        if engine not in ThaiTokenizer.VALID_TOKENIZER_ENGINE:
            raise Exception(
                f'{engine} engine is not supported for Thai tokenizer!')
        self.engine = engine

    def tokenize(self, text: str) -> List[Token]:
        pass


class VietnameseTokenizer(Tokenizer):
    """
    An AllenNLP Tokenizer for Vietnamese based on CRF using the UnderTheSea library.
    Does not perform sentence segmentation. Only feed in single sentences for best performance.
    """

    
    def tokenize(self, text: str) -> List[Token]:
        pass
