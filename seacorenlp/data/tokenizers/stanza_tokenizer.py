from typing import List


from stanza.pipeline.processor import ProcessorVariant
from stanza.models.common.doc import Document, ID, TEXT, MISC
from allennlp.data.tokenizers.token_class import Token
from allennlp.data.tokenizers.tokenizer import Tokenizer



class StanzaTokenizer(Tokenizer):
    """
    A `Tokenizer` that uses stanza tokenizer. It will return allennlp Tokens and will not do sentence splitter.
    if sentence segmentation is needed, it should be implemented as `SentenceSplitter`.
    """

    def __init__(self, lang: str, **kwargs):
        self.stanza_tokenizer = stanza.Pipeline(
            lang=lang,
            processors='tokenize',
            tokenize_no_ssplit=True,
            **kwargs)

    def tokenize(self, text: str) -> List[Token]:
        stanza_doc = self.stanza_tokenizer(text)
        return [
            Token(text=token.text)
            for sentence in stanza_doc.sentences
            for token in sentence.tokens
        ]


class UnderTheSeaStanzaTokenizer(ProcessorVariant):
    '''
    Stanza ProcessorVariant that tokenizes text using UnderTheSea tokenization engine.
    Mainly to replace the default Stanza Vietnamese tokenizer as it does not work well.
    '''

    def __init__(self, config):
        pass

    def process(self, text: str) -> Document:
        sentences = underthesea.sent_tokenize(text)
        tokenized_sentences = [underthesea.word_tokenize(sent) for sent in sentences]
        document = []
        idx = 0
        for sentence in tokenized_sentences:
            sent = []
            for token_id, token in enumerate(sentence):
                stanza_token = {ID: (token_id + 1, ),
                                TEXT: token,
                                MISC: f'start_char={idx}|end_char={idx + len(token)}'}
                sent.append(stanza_token)
                idx += len(token) + 1
            document.append(sent)
        return Document(document, text)
