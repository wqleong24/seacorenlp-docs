from typing import List

from allennlp.data.tokenizers.sentence_splitter import SentenceSplitter


class ThaiSentenceSplitter(SentenceSplitter):
    def __init__(self):
        self.splitter = pythainlp.tokenize.sent_tokenize

    def split_sentences(self, text: str) -> List[str]:
        """
        AllenNLP wrapper for PyThaiNLP sentence segmentation for Thai.
        Uses the 'crfcut' engine by default.
        """
        return self.splitter(text, keep_whitespace=False)


class VietnameseSentenceSplitter(SentenceSplitter):
    def __init__(self):
        self.splitter = underthesea.sent_tokenize

    def split_sentences(self, text: str) -> List[str]:
        """
        AllenNLP wrapper for UnderTheSea sentence segmentation for Vietnamese.
        """
        return self.splitter(text)
