"""Module for segmentation tasks (word tokenization & sentence segmentation).
"""

from allennlp.data.tokenizers.sentence_splitter import SpacySentenceSplitter

from seacorenlp.models import BaseSegmenter
from seacorenlp.data.tokenizers.custom_tokenizer import (
    ThaiTokenizer, VietnameseTokenizer
)
from seacorenlp.data.tokenizers.sentence_splitter import (
    ThaiSentenceSplitter, VietnameseSentenceSplitter
)
from seacorenlp.data.tokenizers.stanza_tokenizer import (
    StanzaTokenizer, UnderTheSeaStanzaTokenizer
)


class Tokenizer(BaseSegmenter):
    """
    Base class to instantiate specific tokenizer for language.

    **Options for library_name:**
      * ``stanza`` (For Indonesian and Vietnamese)
      * ``pythainlp`` (For Thai)
      * ``underthesea`` (For Vietnamese)

    **Defaults available for the following languages:**
      * ``id``: Indonesian
      * ``ms``: Malay
      * ``th``: Thai
      * ``vi``: Vietnamese
    """
    EXTERNAL_LIBRARIES = {
        'stanza': StanzaTokenizer,
        'pythainlp': ThaiTokenizer,
        'underthesea': VietnameseTokenizer
    }
    DEFAULTS = {
        'id': {'class': StanzaTokenizer, 'kwargs': {'lang': 'id'}},
        'ms': {'class': StanzaTokenizer, 'kwargs': {'lang': 'id'}},
        'th': {'class': ThaiTokenizer, 'kwargs': {}},
        'vi': {'class': VietnameseTokenizer, 'kwargs': {}}
    }


class SentenceSplitter(BaseSegmenter):
    """
    Base class to instantiate specific sentence segmenter for language.

    **Options for library_name:**
      * ``pythainlp`` (For Thai)
      * ``underthesea`` (For Vietnamese)

    **Defaults available for the following languages:**
      * ``id``: Indonesian
      * ``ms``: Malay
      * ``th``: Thai
      * ``vi``: Vietnamese
    """
    EXTERNAL_LIBRARIES = {
        'pythainlp': ThaiSentenceSplitter,
        'underthesea': VietnameseSentenceSplitter
    }
    DEFAULTS = {
        'id': {'class': SpacySentenceSplitter, 'kwargs': {'rule_based': True}},
        'ms': {'class': SpacySentenceSplitter, 'kwargs': {'rule_based': True}},
        'th': {'class': ThaiSentenceSplitter, 'kwargs': {}},
        'vi': {'class': VietnameseSentenceSplitter, 'kwargs': {}}
    }
